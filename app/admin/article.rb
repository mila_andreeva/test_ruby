ActiveAdmin.register Article do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :list, :of, :attributes, :on, :model, :text, :title, :image
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    selectable_column
    id_column
    column 'Image', sortable: :image_file_name do |f| link_to f.image_file_name, f.image.url end
    column :created_at
    actions
  end

  form do |f|
    f.input :title
    f.input :text
    f.inputs "Upload" do
      f.input :image, required: true, as: :file
    end
    f.actions
  end

end
