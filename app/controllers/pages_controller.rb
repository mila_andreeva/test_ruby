class PagesController < ApplicationController

  def index
    @articles = Article.all
  end

  def home
    @articles = Article.first(3)
    @offer = Offer.new
  end

  def blog
    @articles = Article.all
  end

  def contacts
    @offer = Offer.new
  end

  def services
    @articles = Article.all
  end

end


