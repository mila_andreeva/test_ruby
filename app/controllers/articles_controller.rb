class ArticlesController < ApplicationController

    # http_basic_authenticate_with name: "milana", password: "1234", except: [:index, :show]
    # before_action :authenticate_user!


  def index
    @articles = Article.all
  end

  def show
    if @article = Article.find_by(id: params[:id])
      render :show
    else
      render plain: "Page not found", status: 404
     end
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(article_params)
  end

  def update
    @article = Article.find(params[:id])
    p article_params
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end


  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end

  private

    def article_params
      params.require(:article).permit(:list, :of, :attributes, :on, :model, :text, :title, :image)
    end
  end



