class OffersController < ApplicationController
  def create
    @offer = Offer.new(offer_params)
    if @offer.save
      flash[:success] = "Ваша заявка отправлена"
    else
      flash[:errors] = "Ваша заявка не отправлена"
    end
    redirect_to contacts_path
  end

  private

  def offer_params
    params.require(:offer).permit(:name, :number, :email)
  end
end
