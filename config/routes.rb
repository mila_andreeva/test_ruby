Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get '/services', to: 'pages#services'
  get '/online_services', to: 'pages#online_services'
  get '/blog', to: 'pages#blog'
  get '/contacts', to: 'pages#contacts'

  resources :offers
  # get 'welcome/index'

  resources :articles

  root 'pages#home'

  resources :articles


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
