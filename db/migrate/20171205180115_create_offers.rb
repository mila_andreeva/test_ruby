class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.string  :name
      t.float   :number
      t.string  :email
      t.timestamps
    end
  end
end

